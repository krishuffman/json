package jsonexample;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;


public class jsonexample {

    public static String studentToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Student   JSONToStudent(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(s, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return student;
    }

    public static void main(String[] args) {

        Student stu = new Student();
        stu.setName("Kris Huffman");
        stu.setGrade(95.7);
        stu.setGpa(3.95);

        String json = jsonexample.studentToJSON(stu);
        System.out.println(json);

        Student stu2 = jsonexample.JSONToStudent(json);
        System.out.println(stu2);
    }

}
