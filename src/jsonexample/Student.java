package jsonexample;

public class Student {

    private String name;
    private double grade;
    private double gpa;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }
    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }
    public String toString() {
        return "Name: " + name + " ~~ Grade: " + grade + " ~~ GPA: " + gpa + " ~~";
    }
}